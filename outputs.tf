output "ecs-instance-role-name" {
  value = aws_iam_role.ecs-instance-role.name
}

output "ecs-instance-profile-id" {
  value = aws_iam_instance_profile.ecs-instance-profile.id
}
