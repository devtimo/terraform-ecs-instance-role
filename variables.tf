variable "ecs-instance-role-name" {
  default = "ecs-instance-role"
}

variable "ecs-instance-profile-name" {
  default = "ecs-instance-profile"
}

